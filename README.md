# Notes
This is a re-creation of the original idea for a Wildstar Addon in-game.

## Game Plot
Since Wildstar is no more, and any development is impossible, this is an attempt
to re-create it in WPF format.

A Chua has been paid to assassinate you, but is willing to fake your death and
report that his mission is complete, for a fee.

His goal is to get as much money out of you as possible for this, and your goal
is to pay as little as possible for your own life. How much is your life worht?

## User Game Flow
User starts off by choosing one of 26 Containers / Cases of Values for payment.

You want to get the 0.01 Case, and make the Chua only take that, instead of the
1,000,000 Case. By taking a chance, you are removing other case values from the 
potential pool of values.

Cases Chosen at Random by Round:

Rounds 1 to 3: 5 Cases
Rounds 4 and 5: 3 Cases
Rounds 6 onwards: 1 Case

The Chua calculates how much he would pay based on the remaining Cases, and 
trying to figure out how much your Case holds.