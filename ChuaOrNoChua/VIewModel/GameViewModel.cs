﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ChuaOrNoChua.ViewModel
{
    public class GameViewModel : ViewModelBase
    {
        ObservableCollection<PriceSlot> _PricesLeft;
        public ObservableCollection<PriceSlot> PricesLeft
        {
            get => _PricesLeft;
            set
            {
                if (_PricesLeft == value) return;
                _PricesLeft = value;
            }
        }

        ObservableCollection<PriceSlot> _PricesRight;
        public ObservableCollection<PriceSlot> PricesRight
        {
            get => _PricesRight;
            set
            {
                if (_PricesRight == value) return;
                _PricesRight = value;
            }
        }

        ObservableCollection<CaseSlot> _CaseCollection;
        public ObservableCollection<CaseSlot> CaseCollection
        {
            get => _CaseCollection;
            set
            {
                if (_CaseCollection == value) return;
                _CaseCollection = value;
                NotifyPropertyChanged(nameof(CaseCollection));
            }
        }

        public GameViewModel()
        {
            // Initialize Values
            SetupBoard();

            CaseClickCommand = new ChooseCaseCommand(this);
            TakeAChanceClick = new TakeAChanceCommand(this);
            TakeOffer = new TakeOfferCommand(this);
            ResetGame = new ResetGameCommand(this);


            /*
            	-- Order of Operations
                -- Original Idea Design Flow of Deal or No Deal
	            -- 1) Randomize Case
	            -- 2) User picks their case (Leaves 25 Cases)
	            -- 3) Rules:	User needs the $0.01 to pay off the assassin
	            -- 				Assassin wants as much money as he can get (opposite of the real game)
	            --				Player is betting that his briefcase has the $0.01 cent in it
	            -- 4) Round 1) User picks 5 Cases (Leaves 20 Cases)
	            -- 5) Assassin Averages the highest amount possible that he asks for payment
	            -- 6) If Chua then the player dies *LULZ* some kind of humor here
	            -- 7) If No Chua then the player goes to next round
	            -- 8) Round 2) User picks 5 Cases (Leaves 15 Cases)
	            -- 9) Next Calculation
	            -- 10) Round 3) User Picks 5 Cases (Leaves 10 Cases)
	            -- 11) Next Calculation
	            -- 12) Round 4) User Picks 3 Cases (Leaves 7 Cases)
	            -- 13) Calc
	            -- 14) Round 5) User Picks 3 Cases (Leaves 4 Cases)
	            -- 15) Calc
	            -- 16) Round 6) User Picks 1 Case (Leaves 3 Cases)
	            -- 17) Calc
	            -- 18) Round 7) User Picks 1 Case (Leaves 2 cases)
	            -- 19) Calc
	            -- 20) At this point, the user can pick the last case
	            -- 21) Calc
	            -- 22) User bets on his case being the $0.01 case vs. the highest remaining case on the board
	            -- 23) Calc
	            -- 24) Reveal
            */
        }

        public ObservableCollection<float> CaseValues { get; set; }
        public ObservableCollection<float> BoardValues { get; set; }
        CaseSlot _ChosenCase;
        public CaseSlot ChosenCase
        {
            get => _ChosenCase;
            set
            {
                if (_ChosenCase == value) return;
                _ChosenCase = value;
                NotifyPropertyChanged(nameof(ChosenCase));
            }
        }

        float _SelectedCaseValue;
        public float SelectedCaseValue
        {
            get => _SelectedCaseValue;
            set
            {
                if (_SelectedCaseValue == value) return;
                _SelectedCaseValue = value;
                NotifyPropertyChanged(nameof(SelectedCaseValue));
            }
        }
       
        public ResetGameCommand ResetGame { get; set; }

        public void SetupBoard()
        {
            // If we are resetting, we need to reset all the values
            if (BoardValues == null) BoardValues = new ObservableCollection<float>();
            BoardValues.Clear();

            if (CaseValues == null) CaseValues = new ObservableCollection<float>();
            CaseValues.Clear();

            if (PricesLeft == null) PricesLeft = new ObservableCollection<PriceSlot>();
            PricesLeft.Clear();

            if (PricesRight == null) PricesRight = new ObservableCollection<PriceSlot>();
            PricesRight.Clear();

            if (ChosenCase != null) ChosenCase = null;

            OfferTaken = false;

            SelectedCaseValue = 0f;
            OfferAmount = 1000000f;
            // Left Slots
            if (PricesLeft == null) PricesLeft = new ObservableCollection<PriceSlot>();
            if (PricesRight == null) PricesRight = new ObservableCollection<PriceSlot>();
            PricesLeft.Clear();

            List<float> StartingValues = new List<float>() {
                0.01f, 1f,5f,10f,25f,50f,75f,100f,200f,300f,400f,500f,750f,
                1000f,5000f,10000f,25000f,50000f,75000f,100000f,200000f,300000f,400000f,500000f,750000f,1000000f
            };

            CaseValues = new ObservableCollection<float>(StartingValues);

            // New Randomize of Values to try to get the likelyhood of the beginning / end values to be chosen
            BoardValues = RandomizeValues(CaseValues);

            int counter = 1;
            foreach(float v in StartingValues)
            {
                if (counter <= 13)
                    PricesLeft.Add(new PriceSlot { Chosen = false, DisplayValue = v });
                else
                    PricesRight.Add(new PriceSlot { Chosen = false, DisplayValue = v });
                counter++;
            }

            NotifyPropertyChanged(nameof(PricesLeft));
            NotifyPropertyChanged(nameof(PricesRight));
            
            // Create the Case Selection Collection
            if (CaseCollection == null) CaseCollection = new ObservableCollection<CaseSlot>();
            CaseCollection.Clear();

            counter = 1;
            foreach(float v in BoardValues)
            {
                CaseCollection.Add(new CaseSlot(v,counter));
                counter++;
            }
            NotifyPropertyChanged(nameof(CaseCollection));

            ChosenCase = new CaseSlot(0f, 0, false);
        }

        private static readonly Random getrandom = new Random();

        public static int GetRandomNumber(int min, int max)
        {
            lock (getrandom) // synchronize
            {
                return getrandom.Next(min, max);
            }
        }

        ObservableCollection<float> RandomizeValues(ObservableCollection<float> baseValues)
        {
            // Create Collection to be Returned
            ObservableCollection<float> newValues = new ObservableCollection<float>();

            // Pick a number between 1 and the count of baseValues
            int splitPos = GetRandomNumber(0, baseValues.Count - 1);

            // Skip the random position, and take that position + 1
            // to the end of the list as section 1
            // Join beginning to the random position as section 2
            // ---
            // Reason: Attempt to make it more likely that a 1,000,000 and 0.01
            //      Value would be selected in a random shuffle.
            //      The First and Last elements are rarely chosen, so randomizing the first and last elements
            //      is another way to add variety very simply into the shuffle of the case values

            List<float> splitList = new List<float>();
            splitList.AddRange(baseValues.Skip(splitPos));
            splitList.AddRange(baseValues.Take(splitPos));

            // 26 Cases now split, so we randomly add values to new list, and remove that index from the splitList
            while (splitList.Count > 0)
            {
                int num = splitList.Count;
                int idx = GetRandomNumber(0, num - 1);
                newValues.Add(splitList[idx]);
                splitList.RemoveAt(idx);
            }
            return newValues;
        }

        // Button Click Case

        public ChooseCaseCommand CaseClickCommand { get; set; }

        public void ChooseCase(CaseSlot value)
        {
            ChosenCase = value;
            ChosenCase.Chosen = true;

            BoardValues.RemoveAt(BoardValues.IndexOf(ChosenCase.GetCaseValue));

            // Remove Selection from possible random entries
            NotifyPropertyChanged(nameof(BoardValues));

            // Update the Chosen Case to trigger the UI Data Triggers
            NotifyPropertyChanged(nameof(ChosenCase));
        }

        // Take A Chance

        public TakeAChanceCommand TakeAChanceClick { get; set; }

        float _OfferAmount;
        public float OfferAmount
        {
            get => _OfferAmount;
            set
            {
                if (_OfferAmount == value) return;
                _OfferAmount = value;
                NotifyPropertyChanged(nameof(OfferAmount));
            }
        }

        public void TakeAChance()
        {
            if (BoardValues.Count < 2)
            {
                // End of Game

                SelectedCaseValue = ChosenCase.GetCaseValue;
                NotifyPropertyChanged(nameof(SelectedCaseValue));
                OfferTaken = true;
                // Enable Reset of Game
                return;
            }

            if(BoardValues.Count > 10) // First - Third Rounds
            {
                // Take 5 Cases
                for(int i=1;i<=5;i++)
                    ChooseACase();
                Console.WriteLine("--");
                
            } else if(BoardValues.Count > 4) // Fourth and Fifth Rounds
            {
                // Take 3 Cases
                for (int i = 1; i <= 3; i++)
                    ChooseACase();
                Console.WriteLine("--");
            } else // Remaining Cases are taken in singles
            {
                ChooseACase();
                Console.WriteLine("--");
            }
            
            
            NotifyPropertyChanged(nameof(BoardValues));
            NotifyPropertyChanged(nameof(CaseValues));

            // Calculate Offer
            // TODO: Refine this calculation to be as accurate as possible

            // Dealer's Offer Should Always Be As High As Possible
            // Dealer's Offer Should never be lower than the lowest amount on the board

            // Calculate the Average between the highest and lowest numbers based on how many cases are left
            var lowSum = PricesLeft.Where(x => x.Chosen == false).Sum(x => x.DisplayValue);
            var highSum = PricesRight.Where(x => x.Chosen == false).Sum(x => x.DisplayValue);

            var mean = (lowSum + highSum) / PricesLeft.Where(x => x.Chosen == false).Concat(PricesRight.Where(x => x.Chosen == false)).Count();

            var amount = mean / 1.17f;

            // Incase the Offer calculated is lower than the lowest amount, we will add 26% to the lowest amount as the Offer
            if (OfferAmount < amount)
                amount = PricesLeft.Where(x => x.Chosen == false).Concat(PricesRight.Where(x => x.Chosen == false)).OrderBy(x => x.DisplayValue).FirstOrDefault().DisplayValue * 1.26f;

            OfferAmount = amount;
            
            #region Original Offer Calculation

            //float lowSum = 0;
            //int remainingLeft = 0;
            //if (PricesLeft.Where(x => x.Chosen == false).Count() > 0)
            //{
            //    lowSum = PricesLeft.Where(x => x.Chosen == false).Sum(x => x.DisplayValue);
            //    remainingLeft = PricesLeft.Where(x => x.Chosen == false).Count();
            //}

            //float lowOffer = 0f;
            //if (remainingLeft > 0)
            //{
            //    lowOffer = lowSum * 0.17f;
            //}

            //float highSum = 0;
            //int remainingRight = 0;

            //float sumRight = PricesRight.Where(x => x.Chosen == false).Select(x => x.DisplayValue).Sum();
            //if (PricesRight.Where(x => x.Chosen == false).Count() > 0)
            //{
            //    highSum = PricesRight.Where(x => x.Chosen == false).Sum(x => x.DisplayValue);
            //    remainingRight = PricesLeft.Where(x => x.Chosen == false).Count();
            //}

            //float highOffer = 0f;
            //if(remainingRight > 0)
            //{
            //    highOffer = highSum * 0.26f;
            //}

            //if(remainingRight + remainingLeft >= 20)
            //{
            //    OfferAmount = lowSum * 0.17f;
            //} else
            //{
            //    OfferAmount = (lowSum * 0.26f) + (highSum * 0.17f);
            //}

            #endregion
        }

        void ChooseACase()
        {

            Console.WriteLine($"Taking a Case [ {BoardValues.Count} ] Remaining.");
            int choiceIdx = GetRandomNumber(0, BoardValues.Count - 1);
            float number = BoardValues[choiceIdx];
            if (number <= 750f)
            {
                // Left
                PricesLeft[PricesLeft.IndexOf(PricesLeft.Where(x => x.DisplayValue == number).FirstOrDefault())].Chosen = true;
                PricesLeft = new ObservableCollection<PriceSlot>(PricesLeft);
                NotifyPropertyChanged(nameof(PricesLeft));
            }
            else if (number > 750f)
            {
                // Right
                PricesRight[PricesRight.IndexOf(PricesRight.Where(x => x.DisplayValue == number).FirstOrDefault())].Chosen = true;
                PricesRight = new ObservableCollection<PriceSlot>(PricesRight);
                NotifyPropertyChanged(nameof(PricesRight));
            }
            else
            {
                throw new Exception("Invalid Case Selection");
            }

            BoardValues.Remove(number);
        }

        public TakeOfferCommand TakeOffer { get; set; }
        bool _OfferTaken;
        public bool OfferTaken
        {
            get => _OfferTaken;
            set
            {
                if (_OfferTaken == value) return;
                _OfferTaken = value;
                NotifyPropertyChanged(nameof(OfferTaken));
            }
        }

        public void TakeTheOffer()
        {
            OfferTaken = true;
            SelectedCaseValue = ChosenCase.GetCaseValue;
            NotifyPropertyChanged(nameof(SelectedCaseValue));
        }
    }
}
