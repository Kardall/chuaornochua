﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using ChuaOrNoChua.ViewModel;

namespace ChuaOrNoChua
{
    public class ChooseCaseCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        GameViewModel View;

        public ChooseCaseCommand(GameViewModel _view)
        {
            View = _view;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            var btn = parameter as CaseSlot;
            View.ChooseCase(btn);
        }
    }
}
