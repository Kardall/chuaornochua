﻿using ChuaOrNoChua.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ChuaOrNoChua
{
    public class TakeOfferCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        GameViewModel View;

        public TakeOfferCommand(GameViewModel _view)
        {
            View = _view;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            View.TakeTheOffer();
        }
    }
}
