﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuaOrNoChua
{
    public class PriceSlot
    {
        public bool Chosen { get; set; }
        public float DisplayValue { get; set; }
    }
}
