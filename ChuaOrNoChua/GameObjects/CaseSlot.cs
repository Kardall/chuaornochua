﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuaOrNoChua
{
    public class CaseSlot : ViewModelBase
    {
        public bool Chosen { get; set; }
        public int CaseNumber { get; set; }
        float CaseValue { set; get; }

        public CaseSlot(float _cValue, int _cNumber, bool _chosen = false)
        {
            CaseValue = _cValue;
            CaseNumber = _cNumber;
            Chosen = _chosen;
        }

        public float GetCaseValue => CaseValue;
    }
}
